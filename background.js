// la difference entre background et script -> script est appelé lorsque l'on clique sur la popup, le background s'execute en fond

// mise en place de l'icone de base
chrome.browserAction.setIcon({
  path : {
    "19": "img/lock19.png",
    "38": "img/lock38.jpg"
  }
});
// requete XMLHTPP
var xhr = new XMLHttpRequest();
xhr.open("GET", "https://api.twitch.tv/helix/streams/esl_locklear?client_id=wsgr53qmnhauqky30o5ykhnqbqx8b2", true); // appel de l'api twitch
xhr.onreadystatechange = function(channel) {
  if(xhr.readyState == 4) {
    var data = JSON.parse(xhr.responseText);
    if(data["stream"] == null){ // si le stream est online
      chrome.browserAction.setIcon({
        path : {
          "19": "img/lock19.png",
          "38": "img/lock38.jpg"
        }
      });
    }else{ // si le stream est offline
      chrome.browserAction.setIcon({
        path : {
          "19": "img/lockvert19.png",
          "38": "img/lockvert38.png"
        }
      });
      // notification si il est en live
      var notification = new Notification('Locklear est en live !', {
       icon: 'img/lock.jpg',
          body: "Rejoins le pour plus de puissance !",
      });
    }
  }
}
xhr.send();
